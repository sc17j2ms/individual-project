#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <time.h>
#include <chrono>

#define ROW 10
#define COL 10
#define CAPACITY 1000

struct Node {
  int x, y;
  double f, g, h;
};

struct Point {
  int x, y;
};

struct OpenList {
  int x, y;
  double f;
};

double heuristic(Point start, Point end){
  double h = abs(end.x - start.x) + abs(end.y - start.y);
  return h;
}

int exists(Point node, std::vector<OpenList> fringe){
  std::vector<OpenList>::size_type t;
  for(t = 0; t!=fringe.size(); t++){
    if(node.x == fringe[t].x && node.y == fringe[t].y){
      return t;
    }
  }

  return -1;
}

bool exist_in_explored(Point node, std::vector<Point> explored){
  std::vector<OpenList>::size_type t;
  for(t = 0; t!=explored.size(); t++){
    if(node.x == explored[t].x && node.y == explored[t].y){
      return true;
    }
  }
  return false;
}

Point* neighbours(Point node, int grid[ROW][COL], Point Vneighbour[]){

  int i, x_temp, y_temp;
  int count = 0;

  Point moves[4];

  moves[0].x = 1;
  moves[0].y = 0;
  moves[1].x = -1;
  moves[1].y = 0;
  moves[2].x = 0;
  moves[2].y = 1;
  moves[3].x = 0;
  moves[3].y = -1;

  for(i = 0; i < 4; i++){
    x_temp = node.x + moves[i].x;
    y_temp = node.y + moves[i].y;

    if(x_temp < ROW && y_temp < COL){
      if(grid[x_temp][y_temp] != 0){
        Vneighbour[count].x = x_temp;
        Vneighbour[count].y = y_temp;
        count += 1;
      }
      else{
        Vneighbour[count].x = -1;
        Vneighbour[count].y = -1;
        count += 1;
      }
    }
  }

  return Vneighbour;
}

int search(Point node, int cost, int bound, Point end, int grid[ROW][COL]){
  int f = cost+heuristic(node, end);
  int i;
  int min = 9999;

  if(node.x == end.x && node.y == end.y){
    std::cout << node.x << " " << node.y << ", ";
    return -1;
  }
  if(f > bound){
    return f;
  }

  Point Vneighbour[4];
  Point *a = neighbours(node, grid, Vneighbour);

  for(i = 0; i < 4; i++){
    if(a[i].x >= 0 && a[i].y >= 0){
      int temp = search(a[i], cost+grid[a[i].x][a[i].y], bound, end, grid);
      if(temp == -1){
        std::cout << node.x << " " << node.y << ", ";
        return -1;
      }
      if(temp < min){
        min = temp;
      }
    }
  }
  return min;

}

bool IDAStar(Point start, Point end, int grid[ROW][COL]){
  double bound;
  bound = heuristic(start, end);
  while(1){
    int temp = search(start, 0, bound, end, grid);
    if(temp == -1){
      return true;
    }
    if(temp > 100){
      return false;
    }
    bound = temp;
  }
}

bool AStar(int grid[ROW][COL], Point start, Point end) {
  int i, j;
  int new_g;

  Node node_list[ROW][COL];

  for (i=0; i<ROW; i++)
	{
		for (j=0; j<COL; j++)
		{
			node_list[i][j].f = 999;
			node_list[i][j].g = 999;
			node_list[i][j].h = 999;
			node_list[i][j].x= -1;
			node_list[i][j].y = -1;
		}
	}
  std::vector<OpenList>::size_type t;
  std::vector<OpenList> fringe;
  std::vector<Point> explored;

  node_list[start.x][start.y].f = 0;
  node_list[start.x][start.y].g = 0;
  node_list[start.x][start.y].h = 0;
  node_list[start.x][start.y].x = start.x;
  node_list[start.x][start.y].y = start.y;

  OpenList initial;
  initial.x = start.x;
  initial.y = start.y;
  initial.f = 0;

  fringe.push_back(initial);

  while(!fringe.empty()){
    int min = 999999;
    int n = 0;
    for(t = 0; t!=fringe.size(); t++){
      if(fringe[t].f < min){
        n = t;
        min = fringe[t].f;
      }
    }

    if(fringe[n].x == end.x && fringe[n].y == end.y){
      bool path = true;
      int x, y;
      x = fringe[n].x;
      y = fringe[n].y;
      std::cout << "Path is: " << x << " " << y << ", ";

      while(path == true){
        if(node_list[x][y].x == x && node_list[x][y].y == y){
          path = false;
        }
        else{
          x = node_list[x][y].x;
          y = node_list[x][y].y;
          std::cout << x << " " << y << ", ";
        }

      }
      return true;
    }
    Point cur_node;
    cur_node.x = fringe[n].x;
    cur_node.y = fringe[n].y;

    explored.push_back(cur_node);
    fringe.erase(fringe.begin()+(n));

    Point Vneighbour[4];
    Point *a = neighbours(cur_node, grid, Vneighbour);

    for(i = 0; i < 4; i++){
      if(a[i].x >= 0 && a[i].y >= 0){
        if(exist_in_explored(a[i], explored) == true){
          continue;
        }
        new_g = node_list[cur_node.x][cur_node.y].g + grid[a[i].x][a[i].y];
        if(new_g < node_list[a[i].x][a[i].y].g){
          node_list[a[i].x][a[i].y].g = new_g;
          node_list[a[i].x][a[i].y].h = heuristic(a[i], end);
          node_list[a[i].x][a[i].y].f = node_list[a[i].x][a[i].y].g + node_list[a[i].x][a[i].y].h;
          node_list[a[i].x][a[i].y].x = cur_node.x;
          node_list[a[i].x][a[i].y].y = cur_node.y;

          int pos = exists(a[i], fringe);

          if(pos > 0){
              fringe[pos].f = node_list[a[i].x][a[i].y].f;
            }
          if(pos == -1){
              OpenList new_node;
              new_node.x = a[i].x;
              new_node.y = a[i].y;
              new_node.f = node_list[a[i].x][a[i].y].f;
              /*
              std::cout << "Passed test 6.9 " << new_node.x << " " << new_node.y << " " << new_node.f << "\n";
              std::cout << fringe.size() << "\n";
              for(t = 0; t!=fringe.size(); t++){
                std::cout << fringe[t].x << " " << fringe[t].y << " " << fringe[t].f << "\n";
              }*/

              fringe.push_back(new_node);
            }
          }
        }
      }
    }
    return false;
  }


int main() {
  int grid[ROW][COL] =
  {
		{ 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 },
		{ 1, 1, 1, 0, 1, 1, 1, 0, 1, 1 },
		{ 1, 1, 1, 0, 1, 1, 0, 1, 0, 1 },
		{ 0, 0, 1, 0, 1, 0, 0, 0, 0, 1 },
		{ 1, 1, 1, 0, 1, 1, 1, 0, 1, 0 },
		{ 1, 0, 1, 1, 1, 1, 0, 1, 0, 0 },
		{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 1 },
		{ 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 },
		{ 1, 1, 1, 0, 0, 0, 1, 0, 0, 1 }
	};

  Point start;
  Point end;
  start.x = 0;
  start.y = 0;
  end.x = 8;
  end.y = 0;
  auto start_time = std::chrono::steady_clock::now();
  bool run = AStar(grid, start, end);
  if(run == true){
    std::cout << "Found yay :D" << "\n";
  }
  if(run == false){
    std::cout << "Boo fail D:" << "\n";
  }
  auto end_time = std::chrono::steady_clock::now();
  auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  std::cout << "Time: " << diff.count() << " ns \n";

  std::cout << "======================================================\n";

  start_time = std::chrono::steady_clock::now();
  bool run2 = IDAStar(start, end, grid);
  if(run2 == true){
    std::cout << "Found yay :D" << "\n";
  }
  if(run2 == false){
    std::cout << "Boo fail D:" << "\n";
  }
  end_time = std::chrono::steady_clock::now();
  diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  std::cout << "Time: " << diff.count() << " ns \n";

  return 0;
}
