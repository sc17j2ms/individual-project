#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <chrono>
#include <fstream>

//Defining the size of the grid
#define ROW 100
#define COL 100

struct Node {
  int x, y;
  double f, g, h;
};

struct Point {
  int x, y;
};

struct HNode {
  int x, y;
  double f;
};

//Heuristic function used to calculate manhattan distance between two points.
double heuristic(Point start, Point end){
  double h = abs(end.x - start.x) + abs(end.y - start.y);
  return h;
}

//If the node is in the fringe, return the position of that node
int position(Point node, std::vector<HNode> fringe){
  std::vector<HNode>::size_type t;
  for(t = 0; t!=fringe.size(); t++){
    if(node.x == fringe[t].x && node.y == fringe[t].y){
      return t;
    }
  }

  return -1;
}

//Checks if a given node exists within the explored list
bool exists(Point node, std::vector<Point> explored){
  std::vector<HNode>::size_type t;
  for(t = 0; t!=explored.size(); t++){
    if(node.x == explored[t].x && node.y == explored[t].y){
      return true;
    }
  }
  return false;
}

//Returns a list of all valid neighbours of a node on the grid
Point* neighbours(Point node, int grid[ROW][COL], Point Vneighbour[]){

  int i, x_temp, y_temp;
  int count = 0;

  //Initialising list of possible moves
  Point moves[4];

  moves[0].x = 1;
  moves[0].y = 0;
  moves[1].x = -1;
  moves[1].y = 0;
  moves[2].x = 0;
  moves[2].y = 1;
  moves[3].x = 0;
  moves[3].y = -1;


  for(i = 0; i < 4; i++){
    x_temp = node.x + moves[i].x;
    y_temp = node.y + moves[i].y;

    //If move is within the range of the grid and does not collide with obstacle
    //Add node to the list of valid neighbours, otherwise path is not viable
    if(x_temp < ROW && y_temp < COL){
      if(grid[x_temp][y_temp] != 0){
        Vneighbour[count].x = x_temp;
        Vneighbour[count].y = y_temp;
        count += 1;
      }
      else{
        Vneighbour[count].x = -1;
        Vneighbour[count].y = -1;
        count += 1;
      }
    }
  }

  return Vneighbour;
}

//IDA* Search function for searching each node until a bound is met
int search(Point node, int cost, int bound, Point end, int grid[ROW][COL]){
  int f = cost+heuristic(node, end);
  int i;
  int min = 9999;

  //If the node is the end node return -1, representing the end has been found
  if(node.x == end.x && node.y == end.y){
    //std::cout << node.x << " " << node.y << ", ";
    return -1;
  }
  if(f > bound){
    return f;
  }

  //Find all neighbours of the current node searching
  Point Vneighbour[4];
  Point *a = neighbours(node, grid, Vneighbour);

  for(i = 0; i < 4; i++){
    if(a[i].x >= 0 && a[i].y >= 0){
      //repeat search for f value of neighbour
      int temp = search(a[i], cost+grid[a[i].x][a[i].y], bound, end, grid);
      //If the returned value is the end goal, reutnr -1 and path
      if(temp == -1){
        //std::cout << node.x << " " << node.y << ", ";
        return -1;
      }
      //If the f value is smaller then a previously found f value, make min new f
      if(temp < min){
        min = temp;
      }
    }
  }
  return min;

}

//IDA* Algorithm finding the shortest minimum path between two points on a grid
bool IDAStar(Point start, Point end, int grid[ROW][COL]){
  double bound;
  //set the initial bound to the heuristic value of the starting node
  bound = heuristic(start, end);
  //until the function returns a value perform the search. If the search Returns
  //With the end node, return that the path has been found. If the returned value
  //Is past a point return the path is not found. Otherwise set bound to found
  //Value and repeat
  while(1){
    int temp = search(start, 0, bound, end, grid);
    //std::cout << temp << std::endl;
    if(temp == -1){
      return true;
    }
    if(temp > 10000){
      return false;
    }
    bound = temp;
  }
}

//A* Algorithm finding the shortest minimum path between two points on a grid
bool AStar(int grid[ROW][COL], Point start, Point end) {
  int i, j;
  int new_g;

  Node node_list[ROW][COL];

  //Initialise all values in the node list including the starting node
  for (i=0; i<ROW; i++)
	{
		for (j=0; j<COL; j++)
		{
			node_list[i][j].f = 999;
			node_list[i][j].g = 999;
			node_list[i][j].h = 999;
			node_list[i][j].x= -1;
			node_list[i][j].y = -1;
		}
	}

  node_list[start.x][start.y].f = 0;
  node_list[start.x][start.y].g = 0;
  node_list[start.x][start.y].h = 0;
  node_list[start.x][start.y].x = start.x;
  node_list[start.x][start.y].y = start.y;

  std::vector<HNode>::size_type t;
  std::vector<HNode> fringe;
  std::vector<Point> explored;

  //Add the start node to the fringe
  HNode initial;
  initial.x = start.x;
  initial.y = start.y;
  initial.f = 0;

  fringe.push_back(initial);

  //While the fringe is not empty find the node in fringe with the lowest f value
  while(!fringe.empty()){
    int min = 999999;
    int n = 0;
    for(t = 0; t!=fringe.size(); t++){
      if(fringe[t].f < min){
        n = t;
        min = fringe[t].f;
      }
    }

    //if the node found is the end node, return that the node has been found
    //and write the path it took to find the node
    if(fringe[n].x == end.x && fringe[n].y == end.y){
      bool path = true;
      int x, y;
      x = fringe[n].x;
      y = fringe[n].y;
      //std::cout << "Path is: " << x << " " << y << ", ";

      while(path == true){
        if(node_list[x][y].x == x && node_list[x][y].y == y){
          path = false;
        }
        else{
          int temp_x = x;
          int temp_y = y;
          x = node_list[temp_x][temp_y].x;
          y = node_list[temp_x][temp_y].y;
          //std::cout << x << " " << y << ", ";
        }

      }
      return true;
    }

    //add the current node to the explored list and remove it from the fringe
    Point cur_node;
    cur_node.x = fringe[n].x;
    cur_node.y = fringe[n].y;

    explored.push_back(cur_node);
    fringe.erase(fringe.begin()+(n));

    //Find all the valid neighbours for the current node
    Point Vneighbour[4];
    Point *a = neighbours(cur_node, grid, Vneighbour);

    for(i = 0; i < 4; i++){
      if(a[i].x >= 0 && a[i].y >= 0){
        //if the neighbour has already been explored then start at a new neighbour
        if(exists(a[i], explored) == true){
          continue;
        }
        //Find the new cost from getting to the current node to the neighbour
        new_g = node_list[cur_node.x][cur_node.y].g + grid[a[i].x][a[i].y];
        //If the new cost was lower then one previously stored, make this the
        //new path to the neighbour
        if(new_g < node_list[a[i].x][a[i].y].g){
          node_list[a[i].x][a[i].y].g = new_g;
          node_list[a[i].x][a[i].y].h = heuristic(a[i], end);
          node_list[a[i].x][a[i].y].f = node_list[a[i].x][a[i].y].g + node_list[a[i].x][a[i].y].h;
          node_list[a[i].x][a[i].y].x = cur_node.x;
          node_list[a[i].x][a[i].y].y = cur_node.y;

          //Check to see if the node is in the fringe. If it is then update
          //f value with new f value. Otherwise add it to the fringe
          int pos = position(a[i], fringe);

          if(pos > 0){
              fringe[pos].f = node_list[a[i].x][a[i].y].f;
            }
          if(pos == -1){
              HNode new_node;
              new_node.x = a[i].x;
              new_node.y = a[i].y;
              new_node.f = node_list[a[i].x][a[i].y].f;
              fringe.push_back(new_node);
            }
          }
        }
      }
    }
    return false;
  }


int main() {

  /*
  int grid[ROW][COL] =
  {
		{ 1, 2, 1, 1, 1, 1, 1, 1, 1, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 1, 2 },
		{ 1, 0, 0, 0, 0, 0, 0, 1, 0, 2 },
		{ 1, 0, 0, 0, 0, 0, 0, 2, 0, 2 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 1, 2 },
		{ 1, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
		{ 1, 0, 0, 0, 0, 0, 1, 1, 1, 1 },
		{ 1, 1, 1, 1, 1, 1, 1, 0, 0, 1 }
	};*/


  //Opens the file containing the map grid and inserts it into the grid array
  int grid[100][100];
  //std::ifstream f("ObstacleGrid.txt");
//std::ifstream f("DifficultTerrainGrid.txt");
  std::ifstream f("DiffTerrainAndObstacles.txt");

  if(!f){
    std::cout << "error opening file \n";
    system("pause");
    return -1;
  }

  int i, j;
  for(i = 0; i < ROW; i++){
    for(j = 0; j < COL; j++){
      f >> grid[i][j];
    }
  }

  Point start;
  Point end;
  start.x = 0;
  start.y = 0;
  end.x = 89;
  end.y = 89;

  auto start_time = std::chrono::steady_clock::now();
  bool run = AStar(grid, start, end);
  if(run == true){
    std::cout << "A* Path found" << "\n";
  }
  if(run == false){
    std::cout << "A* Path not found" << "\n";
  }
  auto end_time = std::chrono::steady_clock::now();
  auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  std::cout << "Time: " << diff.count() << " ns \n";

  std::cout << "======================================================\n";

  start_time = std::chrono::steady_clock::now();
  bool run2 = IDAStar(start, end, grid);
  if(run2 == true){
    std::cout << "IDA* Path found" << "\n";
  }
  if(run2 == false){
    std::cout << "IDA* Path not found" << "\n";
  }
  end_time = std::chrono::steady_clock::now();
  diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
  std::cout << "Time: " << diff.count() << " ns \n";

  return 0;
}
